
import std/tables
import std/strutils
import std/sequtils

type
  OutputType* = enum
    raw, raw_file, csv, csv_files, json
  Output* = ref object
    case output_type: OutputType
    of raw: discard
    of raw_file: destination_file: File
    of csv:
        csv_column_order: ref Table[string, ref Table[string, int]]
    of csv_files:
        csv_files: ref Table[string, File]
        csv_files_column_order: ref Table[string, ref Table[string, int]]
    of json:
        json_files: ref Table[string, File]
    destination_tag: string

proc parseOutputType*(s: string): OutputType =
  case s
  of "raw": raw
  of "raw_file": raw_file
  of "csv": csv
  of "csv_files": csv_files
  else: raiseAssert "invalid output type: " & s

proc newOutput*(output_type: OutputType, tag: string = ""): Output =
    case output_type
    of raw:
        result = Output(output_type: output_type, destination_tag: tag)
    of raw_file:
        if tag == "":
          raiseAssert "tag may not be empty in this mode"
        var file = open(tag, fmWrite)
        result = Output(output_type: output_type, destination_file: file, destination_tag: tag)
    of csv:
        result = Output(output_type: output_type, csv_column_order: newTable[string, ref Table[string, int]](), destination_tag: tag)
    of csv_files:
        result = Output(output_type: output_type, csv_files: newTable[string, File](), csv_files_column_order: newTable[string, ref Table[string, int]](), destination_tag: tag)
    of json:
        result = Output(output_type: output_type, json_files: newTable[string, File](), destination_tag: tag)

proc getOutputFile(files_table: var ref Table[string, File], main_tag: string, tag: string, suffix: string): File =
    if files_table.hasKey(tag):
        result = files_table[tag]
    else:
        var output_name: string
        if tag == "":
            output_name = main_tag & suffix
        else:
            output_name = main_tag & "." & tag & suffix

        result = open(output_name, fmWrite)
        files_table[tag] = result

proc toCsvTable(values: openArray[(string, string)], column_order: var ref Table[string, int]): seq[string] =
    result = newSeq[string](column_order.len())

    for key, value in items(values):
        if column_order.hasKey(key):
            result[column_order[key]] = value
        else:
            column_order[key] = column_order.len()
            result.add value

proc csv_escape(s: string): string = s.replace("\"", "\"\"")

proc output_csv_file(file: File, column_order: var ref Table[string, int], values: openArray[(string, string)]) =
    let escaped_values = values.toCsvTable(column_order).mapIt(it.csv_escape())
    let joined = '"' & join(escaped_values, "\",\"") & '"'
    file.writeLine(joined)

proc output_csv(tag: string, column_order: var ref Table[string, int], values: openArray[(string, string)]) =
    var escaped_values = values.toCsvTable(column_order).mapIt(it.csv_escape())
    if tag != "":
        escaped_values.insert(tag.csv_escape(), 0)
    let joined = '"' & join(escaped_values, "\",\"") & '"'
    echo joined


proc output*(output: var Output, tag: string, values: openArray[(string, string)]) =
    case output.output_type
    of raw:
        if output.destination_tag.len() == 0 and tag.len() == 0:
            echo $values
        elif tag.len() == 0:
            echo "[" & output.destination_tag & "] " & $values
        elif output.destination_tag.len() == 0:
            echo "[" & tag & "] " & $values
        else:
            echo "[" & output.destination_tag & "," & tag & "] " & $values
    of raw_file:
        if tag.len() == 0:
            output.destination_file.writeLine($values)
        else:
            output.destination_file.writeLine("[" & tag & "] " & $values)
    of csv:
        discard output.csv_column_order.hasKeyOrPut(tag, newTable[string, int]())
        output_csv(tag, output.csv_column_order[tag], values)
    of csv_files:
        let output_file = getOutputFile(output.csv_files, output.destination_tag, tag, ".csv")
        discard output.csv_files_column_order.hasKeyOrPut(tag, newTable[string, int]())
        output_csv_file(output_file, output.csv_files_column_order[tag], values)
    else:
        raiseAssert("not implemented")

proc close*(output: Output) =
    case output.output_type
    of raw, csv:
        discard
    of raw_file:
        output.destination_file.close()
    of csv_files:
        for file in output.csv_files.values():
            file.close()
    else:
        raiseAssert("not implemented")
