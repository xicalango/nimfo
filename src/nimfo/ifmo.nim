
import std/macros
import std/sequtils
import std/tables
import std/options
import regex

type 
  OutputReceiver* = proc(tag: string, values: openArray[(string, string)])

const EVAL_PREFIX = "eval_"

const EVAL_BEGIN_NAME = EVAL_PREFIX & "begin"
const EVAL_END_NAME = EVAL_PREFIX & "end"
const EVAL_BEGIN_FILE_NAME = EVAL_BEGIN_NAME & "_file"
const EVAL_END_FILE_NAME = EVAL_END_NAME & "_file"
const EVAL_RULE_PREFIX = EVAL_PREFIX & "rule_"
const RULE_BODY_PREFIX = "rule_body_"

proc groupNames(r: Regex2): seq[string] =
  result = toSeq(Regex(r).namedGroups.keys)

macro ifmo*(name, body: untyped): untyped =
  
  var typeName: NimNode

  if name.kind == nnkIdent:
    typeName = name
  else:
    error "Invalid node: " & name.lispRepr

  result = newStmtList()

  result.add quote do:
    type `typeName` = object
      output_receiver: OutputReceiver

  result.add quote do:
    proc output(self: `typeName`, tag: string, values: openArray[(string, string)]) =
      self.output_receiver(tag, values)
    
    proc output(self: `typeName`, tag: string, values: openArray[string]) =
      var transformed = newSeq[(string, string)]()

      for i, v in pairs(values):
        transformed.add ($i, v)
      
      self.output(tag, transformed)

    proc output(self: `typeName`, values: openArray[(string, string)]) =
      self.output("", values)

    proc output(self: `typeName`, values: openArray[string]) =
      self.output("", values)


  var eval_proc_counter = 0

  var has_begin = false
  var has_end = false
  var has_begin_file = false
  var has_end_file = false
  var is_begin_only = true

  var recList = result[0][0][2][2]

  let varType = newTree(nnkVarTy, typeName)

  for node in body.children:
    case node.kind:
    of nnkPrefix:
      if eqIdent(node[0], "/") and node[1].kind == nnkStrLit and node[2].kind == nnkStmtList:
        is_begin_only = false

        let regex = re2(strVal(node[1]))
        let regex_group_names = regex.groupNames()

        let rule_body_proc_name = ident(RULE_BODY_PREFIX & $eval_proc_counter)

        var rule_body_proc_params = @[newEmptyNode(), newIdentDefs(ident("self"), varType), newIdentDefs(ident("line"), ident("string"))]

        for group_name in regex_group_names:
          rule_body_proc_params.add newIdentDefs(ident(group_name), ident("string"))

        let rule_body_proc = newProc(rule_body_proc_name, params = rule_body_proc_params, body = node[2])
        result.add rule_body_proc

        let rule_eval_proc_name = ident(EVAL_RULE_PREFIX & $eval_proc_counter)

        let rule_eval_proc = quote do:
          proc `rule_eval_proc_name`(self: var `typeName`, line: string) =
            var m: RegexMatch2
            if match(line, `regex`, m):
              `rule_body_proc_name`() 

        #TODO get rid of magic numbers...
        let self_ident = rule_eval_proc[3][1][0]
        let line_ident = rule_eval_proc[3][2][0]
        let m_ident = rule_eval_proc[6][0][0][0]
        var rule_eval_proc_call = rule_eval_proc[6][^1][^1][^1][^1]

        rule_eval_proc_call.add self_ident
        rule_eval_proc_call.add line_ident

        for group_name in regex_group_names:
          rule_eval_proc_call.add quote do:
            `line_ident`[group(`m_ident`, `group_name`)]

        result.add(rule_eval_proc)

        eval_proc_counter += 1
      else:
        error "Invalid node " & node.lispRepr

    of nnkCall:
      if eqIdent(node[0], "BEGIN"):
        if has_begin:
          error "Begin declared more than once"
        has_begin = true
        result.add newProc(ident(EVAL_BEGIN_NAME), params = @[newEmptyNode(), newIdentDefs(ident("self"), varType)], body = node[1])
      elif eqIdent(node[0], "END"):
        is_begin_only = false
        if has_end:
          error "End declared more than once"
        has_end = true
        result.add newProc(ident(EVAL_END_NAME), params = @[newEmptyNode(), newIdentDefs(ident("self"), varType)], body = node[1])
      elif eqIdent(node[0], "BEGIN_FILE"):
        is_begin_only = false
        if has_begin_file:
          error "BEGIN_FILE declared more than once"
        has_begin_file = true
        result.add newProc(ident(EVAL_BEGIN_FILE_NAME), params = @[newEmptyNode(), newIdentDefs(ident("self"), varType), newIdentDefs(ident("file_name"), ident("string"))], body = node[1])
      elif eqIdent(node[0], "END_FILE"):
        is_begin_only = false
        if has_end_file:
          error "END_FILE declared more than once"
        has_end_file = true
        result.add newProc(ident(EVAL_END_FILE_NAME), params = @[newEmptyNode(), newIdentDefs(ident("self"), varType), newIdentDefs(ident("file_name"), ident("string"))], body = node[1])
      elif eqIdent(node[0], "*"):
        is_begin_only = false
        let rule_body_proc_name = ident(RULE_BODY_PREFIX & $eval_proc_counter)

        var rule_body_proc_params = @[newEmptyNode(), newIdentDefs(ident("self"), varType), newIdentDefs(ident("line"), ident("string"))]
        result.add newProc(rule_body_proc_name, params = rule_body_proc_params, body = node[1])

        let rule_eval_proc_name = ident(EVAL_RULE_PREFIX & $eval_proc_counter)
        var rule_eval_proc  = quote do:
          proc `rule_eval_proc_name`(self: var `typeName`, line: string) =
            `rule_body_proc_name`()
        
        let self_ident = rule_eval_proc[3][1][0]
        let line_ident = rule_eval_proc[3][2][0]
        var rule_body_proc_call = rule_eval_proc[^1][^1]

        rule_body_proc_call.add self_ident
        rule_body_proc_call.add line_ident

        result.add rule_eval_proc

        eval_proc_counter += 1

      else:
        error "Invalid node " & node.lispRepr

    of nnkVarSection:
      for n in node.children:
        recList.add(n)

    else:
      error "Invalid node" & node.lispRepr

  if not has_begin:
    result.add quote do:
      proc eval_begin(self: var `typename`) = discard

  if not has_end:
    result.add quote do:
      proc eval_end(self: var `typename`) = discard

  if not has_begin_file:
    result.add quote do:
      proc eval_begin_file(self: var `typename`, file_name: string) = discard

  if not has_end_file:
    result.add quote do:
      proc eval_end_file(self: var `typename`, file_name: string) = discard
  
  result.add quote do:
    func is_begin_only(self: `typename`): bool = `is_begin_only`

  var all_eval_proc = quote do:
    proc evaluate(self: var `typeName`, line: string) =
      discard

  let self_ident = all_eval_proc[3][1][0]
  let line_ident = all_eval_proc[3][2][0]

  var call_statements = newStmtList()

  for i in 0..eval_proc_counter-1:
    let eval_rule_call_name = ident(EVAL_RULE_PREFIX & $i)

    call_statements.add quote do:
      `eval_rule_call_name`(`self_ident`, `line_ident`)
  
  all_eval_proc[6] = call_statements

  result.add all_eval_proc
