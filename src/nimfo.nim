
import nimfo/output
import nimfo/ifmo
import std/strutils
import std/streams
import std/parseopt

export ifmo

template run*(ifmo: untyped, file_name: string, input: Stream) =
  ifmo.eval_begin_file(file_name)

  for line in input.lines():
    var line = line
    line.stripLineEnd()
    ifmo.evaluate(line)

  ifmo.eval_end_file(file_name)

template run*(ifmo_type: untyped, input_files: seq[string], ifmo_output: var Output) =
  var ifmo = ifmo_type(output_receiver: proc(tag: string, values: openArray[(string, string)]) = output(ifmo_output, tag, values))

  ifmo.eval_begin()

  if not ifmo.is_begin_only():
    for file_name in input_files:
      var input_stream = newFileStream(file_name)
      defer: input_stream.close()
      run(ifmo, file_name, input_stream)
  
  ifmo.eval_end()

template run*(ifmo_type: untyped, ifmo_output: var Output) =
  var ifmo = ifmo_type(output_receiver: proc(tag: string, values: openArray[(string, string)]) = output(ifmo_output, tag, values))

  ifmo.eval_begin()

  if not ifmo.is_begin_only():
    var input_stream = newFileStream(system.stdin)
    run(ifmo, "<stdin>", input_stream)
  
  ifmo.eval_end()


proc run*(ifmo_type: type) =
  var output_type = raw
  var output_tag = ""

  var input_files = newSeq[string]()

  var parser = initOptParser()

  for kind, key, value in parser.getOpt():
    case kind
    of cmdArgument:
      input_files.add key
    of cmdLongOption, cmdShortOption:
      case key
      of "output", "o":
        output_tag = value
      of "type", "t":
        output_type = parseOutputType(value)
    of cmdEnd:
      assert(false)
  
  var output = newOutput(output_type, output_tag)

  if input_files.len() == 0:
    run(ifmo_type, output)
  else:
    run(ifmo_type, input_files, output)
    
