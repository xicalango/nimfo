
import nimfo

ifmo Test:
    BEGIN:
        self.output {"name": "name", "rest": "rest"}

    BEGIN_FILE:
        self.output "file_name", [file_name]

    /"^template (?P<name>[a-zA-Z0-9_]+)(?P<rest>.+)":
        self.output {"name": name, "rest": rest}

    `*`:
        discard

run(Test)
