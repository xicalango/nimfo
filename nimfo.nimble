# Package

version       = "0.1.0"
author        = "Alexander Weld"
description   = "Input FilterMap Output in nim"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 2.0.0"
requires "regex >= 0.22.0"
