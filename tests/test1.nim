# This is just an example to get you started. You may wish to put all of your
# tests into a single file, or separate them into multiple `test1`, `test2`
# etc. files (better names are recommended, just make sure the name starts with
# the letter 't').
#
# To run these tests, simply execute `nimble test`.

import unittest

import nimfo
import nimfo/ifmo
import nimfo/output

ifmo Test:

  var x: int
  
  BEGIN:
    self.x = 1
    echo "begin"
  
  /"(?P<name>bla)":
    echo "hello from inside: ", name
    self.output tag="gna", {"hahaha": name & $self.x}

  `*`:
    echo "i get evaluated always"
    self.output {"always hahaha": $self.x}
    self.output ["a", $1, "c"]

proc newTest(output_receiver: OutputReceiver): Test = Test(output_receiver: output_receiver)

test "ifmo":
  var x = newTest(proc(tag: string, values: openArray[(string, string)]) = echo "output [", tag, "] = ", values)
  x.eval_rule_0("bla")
  x.rule_body_0("line", "value")
  x.eval_rule_0("ahsjkd")

  x.eval_begin()
  x.evaluate("bla")
  x.eval_end()

test "output":
  var output = newOutput(raw, "")
  defer: output.close()

  var x = newTest(proc(tag: string, values: openArray[(string, string)]) = output.output(tag, values))

  x.eval_begin()
  x.evaluate("bla")
  x.eval_end()

test "csv_file_output":
  var output = newOutput(csv_files, "test")
  defer: output.close()

  var x = newTest(proc(tag: string, values: openArray[(string, string)]) = output.output(tag, values))

  x.eval_begin()
  x.evaluate("bla")
  x.eval_end()

test "csv_output":
  var output = newOutput(csv, "test")
  defer: output.close()

  var x = newTest(proc(tag: string, values: openArray[(string, string)]) = output.output(tag, values))

  x.eval_begin()
  x.evaluate("bla")
  x.eval_end()
